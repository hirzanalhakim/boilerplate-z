import {
 About,
 Home, 
 NotFound
} from '../containers';

export const MainRoutes = [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/about-us',
    component: About
  },
  {
    component: NotFound
  }
];