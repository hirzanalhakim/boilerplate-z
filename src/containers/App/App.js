import React, { Component } from 'react';
import { Route, Link, Switch, withRouter } from 'react-router-dom'
import { CustomPropsRoute } from './../../components';
import { connect } from 'react-redux';
import { MainRoutes } from './../../utils/TheRoutes';

// const Apps = () => (
//   <div>
//     <header>
//       <Link to="/">Home</Link>
//       <Link to="/about-us">About</Link>
//     </header>

//     <main>
//       <Route exact path="/" component={Home} />
//       <Route exact path="/about-us" component={About} />
//     </main>
//   </div>
// )

class App extends Component {
  render() {
    return (
      <div>
        <header>
          <Link to="/">Home</Link>
          <Link to="/about-us">About</Link>
        </header>
        <main>
          <Switch>
            {MainRoutes.map((route, key) => {
              return (
                <CustomPropsRoute {...route} key={key} />
              )
            })}
          </Switch>
        </main>

      </div>
    )
  }
}
const mapStateToProps = state => ({
  // user: state.login
});

const mapDispatchToProps = dispatch => ({
  // userAction: bindActionCreators(userAction, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));