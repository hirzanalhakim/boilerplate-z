export { default as About } from './About/About';
export { default as App } from './App/App';
export { default as Home } from './Home/Home';
export { default as NotFound } from './NotFound/NotFound';